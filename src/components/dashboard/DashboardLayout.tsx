import React from "react";
import DashboardHeader from "./header/DashboardHeader";
import { Outlet } from "react-router-dom";
import DashboardFooter from "./footer/DashboardFooter";

function DashboardLayout() {
  return (
    <>
      <DashboardHeader />
      <div className="mt-[115px] min-h-screen w-full">
        <Outlet />
      </div>
      <DashboardFooter />
    </>
  );
}

export default DashboardLayout;
