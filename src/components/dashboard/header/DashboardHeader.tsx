import React from "react";
import { NavLink } from "react-router-dom";

function DashboardHeader() {
  return (
    <div
      className=" fixed top-0 left-0 min-h-[83px] p-[15px] bg-[#FFFFFF] w-full z-[100] "
      style={{
        boxShadow: "0px 4px 4px 0px rgba(0, 0, 0, 0.25)",
      }}
    >
      <div className="custom-container flex justify-between items-center min-h-[83px] w-full pt-[15px] pb-[15px]">
        <div className="left flex lg:gap-x-[40px] justify-start items-center ">
          <div className=" brand-logo">
            <img
              src="/dashboard/login/Logo.png"
              className=" w-[196px] object-cover"
              alt="brand-logo"
            />
          </div>
        </div>

        <div className=" login-section  flex justify-end">
          <div className=" flex gap-x-[19px] justify-between items-center">
            <div className="left ">
              <p className=" text-[#808080] font-[600] text-[16px]">Username</p>
            </div>
            <div className="right ">
              <NavLink to={"/login"}>
                <img
                  src="/website/home/loginIcon.png"
                  className=" w-[50px] h-[50px] object-cover"
                  alt="user-icon"
                />
              </NavLink>
            </div>
            <div className="right ">
              <NavLink to={"/login"}>
                <img
                  src="/dashboard/home/sign-out.png"
                  className=" w-[30px] h-[30px] object-cover"
                  alt="user-icon"
                />
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DashboardHeader;
