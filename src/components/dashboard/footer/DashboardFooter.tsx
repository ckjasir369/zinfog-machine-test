import React from "react";

function DashboardFooter() {
  return (
    <section className=" bg-[#E4FBFB] p-[47px] mt-[20px]">
      <div className="flex justify-between">
        <div className="left font-[400]">
          <p>Copyright © 2023 Access Home Lab Solutions</p>
        </div>
        <div className="center">
          <a href="tel:+919288008801" className="flex items-center gap-x-[6px]">
            <img
              src="/dashboard/login/Phone.png"
              alt="phone-logo"
              width={30}
              height={30}
              className="w-[30px] object-contain"
            />
            <p className="text-[24px] text-[#1F6CAB] font-[700]">
              (+91) 9288008801
            </p>
          </a>
        </div>
        <div className="right">
          <p className=" text-[#4bf1f7] font-[400]">
            All Rights Reserved | Terms and Conditions | Privacy Policy
          </p>
        </div>
      </div>
    </section>
  );
}

export default DashboardFooter;
