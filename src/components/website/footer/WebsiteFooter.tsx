import React from "react";

function WebsiteFooter() {
  return (
    <section className="footer-section bg-[#FFFFFF] ">
      <div className="custom-container border-t-2 border-[#c4c2c2d0]">
        <div className="ps-5 lg:ps-[80px] pt-[70px] lg:pt-[121px] pb-[91px]  border-b-2 border-[#C4C4C4]">
          <div className="lg:flex justify-between w-full">
            <div className="left lg:w-[30%]">
              <div className=" brand-logo">
                <img
                  src="/website/home/brandLogo.png"
                  className=" w-[200px] h-[60px] object-cover"
                  alt="zinfog-brand-logo"
                />
              </div>
              <p className=" mt-[24px] text-[#505050] font-[400] text-[18px] max-w-[312px]">
                Lorem ipsum dolor sit amet consectetur adipiscing elit aliquam
              </p>
              <div className="social-media-box mt-[26px] ">
                <div className=" flex gap-x-[22px] items-center">
                  <a href="#">
                    <img
                      src="/website/home/socialmedia/Facebook.png"
                      className=" max-w-[18px] object-cover"
                      alt=""
                    />
                  </a>
                  <a href="#">
                    <img
                      src="/website/home/socialmedia/Twitter.png"
                      className=" max-w-[18px] object-cover"
                      alt=""
                    />
                  </a>
                  <a href="#">
                    <img
                      src="/website/home/socialmedia/Instagram.png"
                      className=" max-w-[18px] object-cover"
                      alt=""
                    />
                  </a>
                  <a href="#">
                    <img
                      src="/website/home/socialmedia/linkedin.png"
                      className=" max-w-[18px] object-cover"
                      alt=""
                    />
                  </a>
                  <a href="#">
                    <img
                      src="/website/home/socialmedia/yotube.png"
                      className=" max-w-[18px] object-cover"
                      alt=""
                    />
                  </a>
                </div>
              </div>
            </div>
            <div className="right lg:w-[55%] mt-[30px] lg:mt-0">
              <div className=" grid grid-cols-2 lg:grid-cols-3">
                <div className="cities">
                  <p className=" font-[500] text-[#1F6CAB] text-[20px] ">
                    Cities
                  </p>
                  <ul className=" mt-[20px] lg:mt-[40px] flex flex-col  gap-y-[18px]">
                    <li>Bangalore</li>
                    <li>Delhi</li>
                    <li>Noida</li>
                    <li>Hydrabad</li>
                  </ul>
                </div>
                <div className="support">
                  <p className=" font-[500] text-[#1F6CAB] text-[20px] ">
                    Support
                  </p>
                  <ul className=" mt-[20px] lg:mt-[40px] flex flex-col  gap-y-[18px]">
                    <li>Getting started</li>
                    <li>Help center</li>
                    <li>Report a bug</li>
                    <li>Refund policy</li>
                    <li>Call support</li>
                  </ul>
                </div>
                <div className="Contacts-us">
                  <p className=" font-[500] text-[#1F6CAB] text-[20px] ">
                    Contacts us
                  </p>
                  <ul className=" mt-[20px] lg:mt-[40px] flex flex-col  gap-y-[18px]">
                    <li className=" flex gap-x-3 items-center">
                      {" "}
                      <span>
                        <img
                          src="/website/home/socialmedia/Email.png"
                          alt=""
                          className=" w-[16px] object-cover"
                        />
                      </span>
                      info@accesslabz.com
                    </li>
                    <li className=" flex gap-x-3 items-center">
                      {" "}
                      <span>
                        <img
                          src="/website/home/socialmedia/Phone.png"
                          alt=""
                          className=" w-[16px] object-cover"
                        />
                      </span>
                      (+91) 9288008801
                    </li>
                    <li className=" flex gap-x-3 items-baseline max-w-[198px]">
                      {" "}
                      <span>
                        <img
                          src="/website/home/socialmedia/Mark.png"
                          alt=""
                          className=" w-[35px] object-cover"
                        />
                      </span>
                      Door No. 28/69/1, Patturaikkal, Thrissur, Kerala 680008,
                      India
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-3">
          <div className=" lg:flex justify-between p-[25px]">
            <p className=" font-[400]">Copyright © 2022 Access labz</p>
            <p className=" font-[400]  mt-5 lg:mt-0">
              All Rights Reserved |
              <span className=" text-[#1F6CAB] ">
                {" "}
                Terms and Conditions | Privacy Policy
              </span>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default WebsiteFooter;
