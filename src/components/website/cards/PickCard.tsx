import React from 'react'

function PickCard() {
  return (
    <div className="card p-[20px] border border-[#1F6CAB1A] shadow-md">
    <div className="top flex justify-between items-center">
      <p>Garuda</p>
      <div className="rating bg-[#007405] text-white p-[4px] rounded-[3px] flex gap-x-2 items-center justify-center">
        4.3{" "}
        <span>
          <img
            src="/website/home/pick/star.png"
            className="h-[10px] w-[10px] object-cover"
            alt=""
          />
        </span>
      </div>
    </div>
    <div className="location text-[#808080] text-[16px] leading-[20px] pb-[10px] mt-[5px] border-b-2 border-[#F2F2F2]">
      <p>Okhla, Delhi</p>
    </div>
    <div className="mt-[10px] ">
      <h4 className=" text-[24px] text-[#000000] leading-[30px] font-[600]">
        Thyroid profile Total
      </h4>
      <p className=" text-[#808080] font-[300]  mt-[5px]">
        (T3, T4, TSH)
      </p>
    </div>

    <div className="price-section mt-[30px]">
      <div className="flex justify-between ">
        <div className="left">
          <div className="price flex justify-center items-center gap-x-[5px]">
            <h2 className=" text-[#000000] text-[32px] font-[700]">
              399/-
            </h2>
            <p className=" text-[#808080] font-[400] text-[18px]">
              1299/-
            </p>
          </div>
          <div className="mt-[5px] ">
            <div className="offer bg-[#DDFFE4] p-[4px] max-w-[61px] border border-dashed border-[#029923] ">
              <h5 className=" text-[70%OFF] text-[14px] font-[300]  leading-[18px]">
                70%OFF
              </h5>
            </div>
          </div>
        </div>
        <div className="right">
          <h3 className=" text-[#1F6CAB] text-[16px] font-[400]">
            Expected report in 1 Day
          </h3>
          <div className=" flex justify-end">
            <button className=" mt-[10px] w-[127px] h-[42px] bg-[#1F6CAB] rounded-[3px] text-white font-[800] text-[14px] text-center ">
              Book
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  )
}

export default PickCard