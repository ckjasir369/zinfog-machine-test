import React from "react";

function BlogCard() {
  return (
    <div className="card p-[20px] border border-[#1F6CAB1A] shadow-md">
      <img
        src="/website/home/blogimage.jpeg"
        alt=""
        className="h-[233px] object-cover"
      />
      <div className="contents ">
        <h5 className=" text-[#505050] text-[24px] font-[600] mt-[15px]">
          what can we learn today?
        </h5>
        <p className="mt-[4px] text-[#505050] font-[400] text-[18px]">
          is simply dummy text of the printing and typesetting industry. Lorem
          Ipsum has been
        </p>
        <p className=" mt-[15px]  text-[#808080] font-[400] text-[16px] flex gap-x-3 items-center"> <span><img src="/website/home/time.png" alt="time" className=" w-[19px] h-[16px] object-contain" /></span>30 mins ago</p>
      </div>
    </div>
  );
}

export default BlogCard;
