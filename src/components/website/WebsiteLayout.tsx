import React from "react";
import WebsiteHeader from "./header/WebsiteHeader";
import WebsiteFooter from "./footer/WebsiteFooter";
import { Outlet } from "react-router-dom";

function WebsiteLayout() {
  return (
    <>
      <WebsiteHeader />
      <div className=" ">
        <Outlet />
      </div>
      <WebsiteFooter />
    </>
  );
}

export default WebsiteLayout;
