import React from "react";

const steps = [
  {
    number: 1,
    title: "Schedule",
    description: "Schedule a test from your home",
    icon: "/website/home/steps/1.png",
  },
  {
    number: 2,
    title: "Reach lab",
    description: "Phlebotomist drops the test ",
    icon: "/website/home/steps/2.png",
  },
  {
    number: 3,
    title: "Get Report",
    description: "Get the report on WhatsApp",
    icon: "/website/home/steps/3.png",
  },
];

function HowWeWork() {
  return (
    <section className=" bg-[#EFF8FF] rounded-[5px]  custom-container">
      <div className=" pt-[40px] pb-[40px] lg:pt-[60px] lg:pb-[60px]">
        <h4 className=" text-[24px] lg:text-[32px] font-[700] text-[#1F6CAB] leading-[40px] text-center">
          How we work
        </h4>

        <div>
          <div className="lg:flex justify-center items-center  py-10 px-[37px]">
            <div className="lg:flex justify-between items-center  w-full">
              {steps.map((step, index) => (
                <div
                  key={index}
                  className={`relative flex flex-col items-center text-center  my-8 ${
                    index === 2 ? " ms-[-100px] lg:ms-0" : "mx-4"
                  }`}
                >
                  {/* Number Circle */}
                  <div className="relative flex items-center justify-center  ">
                    <img
                      src={`${
                        index === 2
                          ? "/website/home/steps/vector2.png"
                          : "/website/home/steps/vector.png"
                      }`}
                      alt=""
                      className={`${
                        index === 2
                          ? "w-[20rem] lg:w-[20rem]  object-cover "
                          : "w-[28rem] lg:w-[30rem]  object-cover"
                      }`}
                    />
                    <h4 className=" absolute top-[-30px] lg:top-[-20px] left-[-40px] lg:left-[-45px] text-white text-[160px] lg:text-[180px] font-bold">
                      {step.number}
                    </h4>
                    {/* Icon */}
                    <div
                      className={`absolute top-[70px]  transform -translate-x-1/2 ${
                        index === 2
                          ? "left-[50%] lg:left-[50%]"
                          : "left-[38%] lg:left-[35%]"
                      } `}
                    >
                      <img
                        src={step.icon}
                        alt={`${step.title} icon`}
                        className=" w-[50px] h-[50px] lg:w-[70px] lg:h-[70px]"
                      />
                    </div>
                  </div>
                  {/* Step Title */}
                  <h3
                    className={`${
                      index === 2
                        ? "absolute top-[20px]  transform -translate-x-1/2  left-[50%] lg:left-[50%] flex justify-center items-center"
                        : "absolute top-[20px]  transform -translate-x-1/2  left-[38%] lg:left-[35%] flex justify-center items-center"
                    } text-[#1A5DA5] text-xl font-semibold mt-4 bg-[#FFFFFF] rounded-[50px] w-[76px] h-[28px]  "`}
                  >
                    {step.title}
                  </h3>
                  {/* Step Description */}
                  <p
                    className={`absolute bottom-[20px] lg:bottom-[20px]  transform -translate-x-1/2 text-[#505050] mt-2 lg:mt-5 max-w-[114px] text-[12px] lg:text-[14px] leading-[14px] lg:leading-[17px] font-[500] ${
                      index === 2
                        ? "left-[50%] lg:left-[50%]"
                        : "left-[38%] lg:left-[35%]"
                    }`}
                  >
                    {step.description}
                  </p>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default HowWeWork;
