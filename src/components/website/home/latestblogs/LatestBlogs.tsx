/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useRef } from 'react'
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Navigation } from "swiper/modules";
import "swiper/css";
import "swiper/css/pagination";
import BlogCard from '../../cards/BlogCard';

const data = [
    {
        id:1
    },
    {
        id:2
    },
    {
        id:3
    },
    {
        id:4
    },
    {
        id:5
    },
]

function LatestBlogs() {


    const swiperRef = useRef<any>(null);

    const handleNextClick = () => {
      if (swiperRef.current && swiperRef.current.swiper) {
        const swiper = swiperRef.current.swiper;
        swiper.slideNext();
      }
    };
  
    // const handlePrevClick = () => {
    //   if (swiperRef.current && swiperRef.current.swiper) {
    //     const swiper = swiperRef.current.swiper;
    //     swiper.slidePrev();
    //   }
    // };

  return (
    <section className=" custom-container  bg-[#FFFFFF]">
    <div className=" pt-[30px] pb-[30px]">
      <h3 className=" text-[#000000] text-[20px]  font-[500] leading-[25px]">
      Latest blog
      </h3>
      <div className="swiper-section mt-[15px] relative ">
      
        <Swiper
       slidesPerView={2.5}
       ref={swiperRef}
       spaceBetween={29}
       pagination={{
         clickable: true,
       }}
       modules={[Autoplay, Navigation]}
       speed={1000}
      //  autoplay={{
      //    delay: 2000,
      //    disableOnInteraction: false,
      //  }}
       loop
    
       // modules={[Navigation]}
       breakpoints={{
         320: {
           slidesPerView: 1,
           spaceBetween: 20,
         },

         600: {
           slidesPerView: 2,
         },

         768: {
           slidesPerView: 1,
         },

         991: {
           slidesPerView: 2.5,
         },

         1140: {
           slidesPerView: 2.5,
         },

         1366: {
           slidesPerView: 3.5,
         },

         1400: {
           slidesPerView: 3.5,
         },

       }}
      >
        {data &&
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          data?.map((item: any, index: any) => (
            <SwiperSlide key={index} >
          <BlogCard/>
            </SwiperSlide>
          ))}
      </Swiper>

    
<button onClick={handleNextClick} className=" absolute top-[33%] right-0 z-[20] transform -translate-x-1/2  flex justify-center items-center bg-white rounded-[100px] p-[25px] w-[98px] h-[98px] shadow-md ">
  <img src="/website/home/pick/arrow.png" alt="arrow" className=" h-[24px] w-[24px] object-contain" />
</button>
  
      </div>
    </div>
  </section>
  )
}

export default LatestBlogs