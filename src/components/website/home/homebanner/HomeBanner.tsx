/* eslint-disable @typescript-eslint/no-explicit-any */
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper/modules";
import "./HomeBanner.scss";
import "swiper/css";
import "swiper/css/pagination";

const bannerData: any = [
  {
    bannerImage: "/website/home/bannerImage.png",
  },
  {
    bannerImage: "/website/home/bannerImage.png",
  },
  {
    bannerImage: "/website/home/bannerImage.png",
  },
  {
    bannerImage: "/website/home/bannerImage.png",
  },
];

function HomeBanner() {
  //   const swiperRef = useRef<any>(null);

  // const handleNextClick = () => {
  //   if (swiperRef.current && swiperRef.current.swiper) {
  //     const swiper = swiperRef.current.swiper;
  //     swiper.slideNext();
  //   }
  // };

  // const handlePrevClick = () => {
  //   if (swiperRef.current && swiperRef.current.swiper) {
  //     const swiper = swiperRef.current.swiper;
  //     swiper.slidePrev();
  //   }
  // };

  return (
    <>
      <>
        <Swiper
          pagination={{
            clickable: true,
          }}
          speed={1000}
          //   autoplay={{
          //     delay: 2000,
          //     disableOnInteraction: false,
          //   }}
          modules={[Autoplay, Pagination]}
          className="mySwiper "
        >
          {bannerData &&
            bannerData?.map((item: any, index: any) => (
              <SwiperSlide key={index} >
                <section
                  className=" home-banner-section min-h-screen relative pb-[34px]"
                  style={{
                    backgroundImage: `url(${item?.bannerImage})`,
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                  }}
                >
                  <div className="absolute inset-0 bg-[#00000059] z-10"></div>
                  <div className="custom-container relative z-20 ">
                    <h1
                      className="text-[#FFFFFF] text-[30px] lg:text-[48px] font-[600] max-w-[490px] pt-[210px] lg:pt-[110px]"
                      style={{
                        textShadow: "0 4px 6px #00000040", // Adding text shadow
                      }}
                    >
                      We have served more than 7000+ customers
                    </h1>
                    <p
                      className="max-w-[391px] text-[14px] lg:text-[16px] text-[#FFFFFF] font-[300] mt-[15px]"
                      style={{
                        textShadow: "0 4px 6px #00000040", // Adding text shadow
                      }}
                    >
                      Lorem ipsum dolor sit amet consectetur adipiscing elit
                      mollis quisque senectus massa lobortis, scelerisque
                      maecenas sagittis faucibus integer{" "}
                    </p>
                    <div className="mt-[30px] lg:h-[80px] bg-[#FFFFFF] rounded-[5px] p-[20px] lg:p-[30px] w-full lg:flex items-center justify-between">
                      <div className="left lg:w-[20%]">
                        <div className="location-icon flex gap-x-[12px] items-center">
                          <img
                            src="/website/home/locationIcon.png"
                            className=" w-[16px] "
                            alt=""
                          />
                          <p>Bangalore</p>
                        </div>
                      </div>
                      <div className="right relative lg:w-[80%] border-l-2 ps-[15px] lg:ps-[30px] border-[#1F6CAB] mt-5 lg:mt-0">
                        <input
                          type="text"
                          placeholder="Search for test ..."
                          className=" w-full outline-none "
                        />
                        <img
                          src="/website/home/searchIcon.png"
                          className=" absolute top-1/2 right-0 transform -translate-x-1/2 -translate-y-1/2  w-[18px]  object-cover"
                          alt=""
                        />
                      </div>
                    </div>
                    <div className="mt-[30px] text-[#FFFFFF] font-[300] ">
                      <p className="text-[14px] lg:text-[16px]">
                        We are associated with more than 25+ lab.
                      </p>
                    </div>
                  </div>
                </section>
              </SwiperSlide>
            ))}
        </Swiper>
      </>
    </>
  );
}

export default HomeBanner;
