import React from "react";

function OfferSection() {
  return (
    <section className=" custom-container">
      <div className=" py-10">
        <section className="relative flex justify-center items-center bg-white overflow-hidden">
          {/* Half Circle Left */}
          <div className="absolute left-0 top-1/2 transform -translate-y-1/2 -translate-x-1/2 w-[70px] h-[80px] lg:w-[100px] lg:h-[100px] bg-white rounded-full z-[30]"></div>

          {/* Image */}
          <img
            src="/website/home/offer/offerimage.jpeg"
            className="h-[280px] w-full object-cover relative z-10"
            alt="Offer"
          />

          <div className="hidden lg:block offer-box bg-[#1F6CAB]  pt-[60px] ps-[50px] absolute top-0 right-0 h-[280px] w-[35%]  z-[25]">
            <h3 className=" text-white text-[32px] font-[700] leading-[40px]">
              GET
            </h3>
            <h2 className=" text-white text-[64px] font-[900] leading-[80px] ">
              50% OFF
            </h2>
            <h3 className=" text-white text-[32px] font-[500] leading-[40px]">
              For new user
            </h3>
          </div>
          <div className="block lg:hidden offer-box bg-[#1F6CAB]  pt-[10px] lg:pt-[60px] ps-[25px] lg:ps-[50px] absolute bottom-0 right-0 h-[120px] w-[100%]  z-[25]">
            <h3 className=" text-white text-[16px] lg:text-[32px] font-[700] leading-[40px]">
              GET
            </h3>
            <h2 className=" text-white text-[32px] lg:text-[64px] font-[900] leading-[40px] lg:leading-[80px] ">
              50% OFF
            </h2>
            <h3 className=" text-white text-[16px] lg:text-[32px] font-[500] leading-[20px] lg:leading-[40px]">
              For new user
            </h3>
          </div>

          {/* Half Circle Right */}
          <div className="absolute right-0 top-1/2 transform -translate-y-1/2 translate-x-1/2 w-[70px] h-[80px] lg:w-[100px] lg:h-[100px] bg-white rounded-full z-[30]"></div>
        </section>
      </div>
    </section>
  );
}

export default OfferSection;
