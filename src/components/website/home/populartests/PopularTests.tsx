/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";

const popularTests = [
  "Liver Function Test",
  "Vitamin D (25-OH)",
  "CBC",
  "Diabetes Screening",
  "COVID-RTPCR",
  "Thyroid profile Total",
  "Thyroid profile Total",
  "Liver Function Test",
  "Vitamin D (25-OH)",
  "CBC",
  "Diabetes Screening",
  "COVID-RTPCR",
  "Thyroid profile Total",
  "Thyroid profile Total",
  "CBC",
];

function PopularTests() {
  return (
    <section className="popular-labs-section custom-container">
      <div className=" pt-[40px] pb-[40px] lg:pt-[60px] lg:pb-[60px]">
        <h4 className=" text-[24px] lg:text-[32px] font-[700] text-[#505050] leading-[40px] text-center">
          Popular test in your city
        </h4>
        <div className="labs-box flex  justify-center flex-wrap  gap-[10px] lg:gap-[15px] p-[30px] lg:p-[60px]  ">
          {popularTests &&
            popularTests.map((item: any, index: any) => (
              <div
                key={index}
                className="flex gap-x-[10px] items-center bg-[#F5F7F8] rounded-[50px] ps-[5px] pt-[10px] pb-[10px] pr-[10px]"
              >
                <img
                  src="/website/home/test-icon.png"
                  alt={`${index}-item`}
                  className=" max-w-[24px] max-h-[24px] lg:max-w-[32px] lg:max-h-[32px] object-contain"
                />
                <p className=" text-[#505050] text-[14px] lg:text-[18px]">
                  {item}
                </p>
              </div>
            ))}
        </div>
      </div>
    </section>
  );
}

export default PopularTests;
