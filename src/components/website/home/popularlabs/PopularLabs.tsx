/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";

const popularLabs = [
  {
    logo: "/website/home/popularlabs/Aayush logo.png",
  },
  {
    logo: "/website/home/popularlabs/aswini logo.png",
  },
  {
    logo: "/website/home/popularlabs/Aza.png",
  },
  {
    logo: "/website/home/popularlabs/BMH.png",
  },
  {
    logo: "/website/home/popularlabs/Calicut Hospital.png",
  },
  {
    logo: "/website/home/popularlabs/Care.png",
  },
  {
    logo: "/website/home/popularlabs/CPC logo.png",
  },
  {
    logo: "/website/home/popularlabs/Dane.png",
  },
  {
    logo: "/website/home/popularlabs/Daya.png",
  },
  {
    logo: "/website/home/popularlabs/DDRC logo.png",
  },
  {
    logo: "/website/home/popularlabs/Fathima Hospital.png",
  },
  {
    logo: "/website/home/popularlabs/Garuda.png",
  },
  {
    logo: "/website/home/popularlabs/Hygea.png.png",
  },
  {
    logo: "/website/home/popularlabs/Iqraa.png",
  },
  {
    logo: "/website/home/popularlabs/Jeeva.png",
  },
  {
    logo: "/website/home/popularlabs/Aayush logo.png",
  },
  {
    logo: "/website/home/popularlabs/aswini logo.png",
  },
  {
    logo: "/website/home/popularlabs/Aza.png",
  },
  {
    logo: "/website/home/popularlabs/BMH.png",
  },
  {
    logo: "/website/home/popularlabs/Calicut Hospital.png",
  },
  {
    logo: "/website/home/popularlabs/Care.png",
  },
  {
    logo: "/website/home/popularlabs/Daya.png",
  },
  {
    logo: "/website/home/popularlabs/DDRC logo.png",
  },
  {
    logo: "/website/home/popularlabs/Fathima Hospital.png",
  },
  {
    logo: "/website/home/popularlabs/Garuda.png",
  },
  {
    logo: "/website/home/popularlabs/Hygea.png.png",
  },
  {
    logo: "/website/home/popularlabs/Iqraa.png",
  },
  {
    logo: "/website/home/popularlabs/Calicut Hospital.png",
  },
  {
    logo: "/website/home/popularlabs/Care.png",
  },
  {
    logo: "/website/home/popularlabs/CPC logo.png",
  },
];

function PopularLabs() {
  return (
    <section className="popular-labs-section custom-container">
      <div className=" pt-[40px] pb-[40px] lg:pt-[60px] lg:pb-[60px]">
        <h4 className=" text-[24px] lg:text-[32px] font-[700] text-[#505050] leading-[40px] text-center">
          Popular Lab in your city
        </h4>
        <div className="labs-box grid grid-cols-3 lg:grid-cols-10 gap-[20px] lg:gap-[30px] p-[30px] lg:p-[60px]  ">
          {popularLabs &&
            popularLabs.map((item: any, index: any) => (
              <div key={index} className="flex justify-center items-center">
                <img
                  src={item?.logo}
                  alt={`${index}-item`}
                  className="max-w-[80px] max-h-[80px] object-contain"
                />
              </div>
            ))}
        </div>
      </div>
    </section>
  );
}

export default PopularLabs;
