/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import {
  Disclosure,
  DisclosureButton,
  DisclosurePanel,
} from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/20/solid";

const faqdata = [
  {
    question: "What is ACCESS home lab collection service?",
    answers:
      " We are the home collection service provider across different cities in Kerala. We simplify lab testing for customers by collecting the test samples from their home or office & getting those tested at any lab of customer's choice.",
  },
  {
    question: "How can I book a home collection?",
    answers:
      " We are the home collection service provider across different cities in Kerala. We simplify lab testing for customers by collecting the test samples from their home or office & getting those tested at any lab of customer's choice.",
  },
  {
    question: "Is there any collection charge?",
    answers:
      " We are the home collection service provider across different cities in Kerala. We simplify lab testing for customers by collecting the test samples from their home or office & getting those tested at any lab of customer's choice.",
  },
  {
    question: "Can I get covid tests at home?",
    answers:
      " We are the home collection service provider across different cities in Kerala. We simplify lab testing for customers by collecting the test samples from their home or office & getting those tested at any lab of customer's choice.",
  },
  {
    question: "How long does it take to get the test reports?",
    answers:
      " We are the home collection service provider across different cities in Kerala. We simplify lab testing for customers by collecting the test samples from their home or office & getting those tested at any lab of customer's choice.",
  },
];

function FaqSection() {
  return (
    <section className=" bg-[#FFFFFF] rounded-[5px]  custom-container relative">
      <div className="custom-container">
        <div className=" pt-[40px] pb-[40px] lg:pt-[60px] lg:pb-[60px] ">
          <h4 className=" leading-[60px] text-[32px] lg:text-[48px] font-[600] text-[#1F6CAB]  text-center">
            Frequently Asked Questions
          </h4>

          <div className="faq-section mt-[30px]">
            <div className=" w-full">
              <div className="mx-auto w-full  max-w-7xl  divide-white/5 rounded-xl bg-white/5">
                {faqdata &&
                  faqdata?.map((item: any, index: any) => (
                    <Disclosure
                      key={index}
                      as="div"
                      className="p-[15px]"
                      defaultOpen={true}
                    >
                      <div className="p-6 border border-[#C4C4C4]  rounded-[5px]">
                        <DisclosureButton className="group flex w-full items-center justify-between">
                          <span className=" text-[18px] lg:text-[24px] font-[600]  leading-[30px]  text-[#505050] group-data-[hover]:text-black">
                            Q1. {item?.question}
                          </span>
                          <ChevronDownIcon className="size-[20px] lg:size-[24px] fill-black/60 group-data-[hover]:fill-black/50 group-data-[open]:rotate-180" />
                        </DisclosureButton>
                        <DisclosurePanel className="mt-2 text-[16px] lg:text-[18px] font-[400] text-[#505050]">
                          {item?.answers}
                        </DisclosurePanel>
                      </div>
                    </Disclosure>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default FaqSection;
