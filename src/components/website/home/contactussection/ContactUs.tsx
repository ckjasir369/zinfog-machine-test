import React from "react";

function ContactUs() {
  return (
    <section className=" bg-[#EFF8FF] rounded-[5px]  custom-container relative">
      <div className=" pt-[40px] pb-[40px] lg:pt-[60px] lg:pb-[60px]">
        <h4 className=" leading-[60px] text-[32px] lg:text-[48px] font-[700] text-[#1F6CAB]  text-center">
          Feel free to contact us
        </h4>
        <div className=" flex justify-center">
          <p className=" text-[#000000] text-[18px] font-[400] mt-[15px]  leading-[22px] max-w-[800px] text-center">
            Lorem ipsum dolor sit amet consectetur adipiscing elit mollis
            quisque senectus massa lobortis, scelerisque maecenas sagittis
            faucibus integer{" "}
          </p>
        </div>
        <div className=" flex justify-center">
          <button className=" bg-[#1F6CAB]  w-[156px] h-[59px]  mt-[40px]  text-[#FFFFFF] rounded-[5px]">
            Contact us
          </button>
        </div>

        <div></div>
      </div>
      <div className="plus-big absolute top-[20px] lg:top-[40px] left-[5%]">
        <img
          src="/dashboard/login/PlusIcon.png"
          alt="plus-icon"
          className="h-[25px] lg:h-[50px] object-contain"
        />
      </div>
      <div className="plus-big absolute bottom-[20px] left-[10%] lg:left-[18%]">
        <img
          src="/dashboard/login/PlusIcon.png"
          alt="plus-icon"
          className="h-[60px] lg:h-[100px] object-contain"
        />
      </div>
      <div className="plus-big absolute bottom-0 right-2 lg:right-[15px]">
        <img
          src="/website/home/big-plus.png"
          alt="plus-icon"
          className="h-[100px] lg:h-[200px] object-cover"
        />
      </div>
    </section>
  );
}

export default ContactUs;
