import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./websiteHeader.scss";

function WebsiteHeader() {
  const [isMenuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
    // document.body.classList.add("toggle-body");
  };

  const closeMenu = () => {
    setMenuOpen(false);
    // document.body.classList.remove("toggle-body");
  };

  return (
    <div className=" fixed top-0 left-0 min-h-[83px] p-[15px] bg-[#F5F7F8] w-full z-[100] ">
      <div className="custom-container flex justify-between items-center min-h-[83px] w-full pt-[15px] pb-[15px]">
        <div className="left flex lg:gap-x-[40px] justify-start items-center ">
          <div className=" brand-logo">
           <NavLink to={'/'}>
              <img
                src="/website/home/brandLogo.png"
                className=" w-[200px] h-[60px] object-cover"
                alt="zinfog-brand-logo"
              />
           </NavLink>
          </div>
          <div className=" navbar-list hidden lg:block  ">
            <ul className=" flex justify-between gap-x-[30px] text-[#808080] ">
              <li className="border-r pe-[30px] border-[#C4C4C4] cursor-pointer hover:text-[#1F6CAB]">
                <NavLink
                  to="/"
                  end
                  className={({ isActive }) =>
                    isActive ? "text-[#1F6CAB]" : ""
                  }
                >
                  Home
                </NavLink>
              </li>
              <li className="border-r pe-[30px] border-[#C4C4C4] cursor-pointer hover:text-[#1F6CAB]">
                <NavLink
                  to="/about"
                  className={({ isActive }) => (isActive ? "active" : "")}
                >
                  About
                </NavLink>
              </li>
              <li className="border-r pe-[30px] border-[#C4C4C4] cursor-pointer hover:text-[#1F6CAB]">
                <NavLink
                  to="/blog"
                  className={({ isActive }) => (isActive ? "active" : "")}
                >
                  Blog
                </NavLink>
              </li>
              <li className="border-r pe-[30px] border-[#C4C4C4] cursor-pointer hover:text-[#1F6CAB]">
                <NavLink
                  to="/get-the-app"
                  className={({ isActive }) => (isActive ? "active" : "")}
                >
                  Get the app
                </NavLink>
              </li>
              <li className="cursor-pointer hover:text-[#1F6CAB]">
                <NavLink
                  to="/add-lab"
                  className={({ isActive }) => (isActive ? "active" : "")}
                >
                  Add lab
                </NavLink>
              </li>
            </ul>
          </div>
        </div>

        <div className=" login-section  flex justify-end">
          <div className=" flex gap-x-[19px] justify-between items-center">
            <div className="left hidden lg:block">
              <NavLink to={'/login'}><p className=" text-[#808080]">Login/sign up</p></NavLink>
            </div>
            <div className="right hidden lg:block">
              <NavLink to={'/login'}>
                <img
                  src="/website/home/loginIcon.png"
                  className=" w-[50px] h-[50px] object-cover"
                  alt="user-icon"
                />
              </NavLink>
            </div>
            <div className=" lg:hidden">
              <button onClick={toggleMenu}>
                <img
                  src="/website/home/menu.png"
                  className=" w-[26px] object-cover"
                  alt="menu"
                />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className={` lg:hidden header_mobile ${isMenuOpen ? "open" : ""}`}>
        <div className="header_flex">
          <div className="header_close ms-5 mt-5">
            <button className="close-btn" onClick={closeMenu}>
              <img
                src="/website/home/close.png"
                width={16}
                height={16}
                className="h-[16px] w-[16px] object-cover"
                alt="close "
              />
            </button>
          </div>
          <div className=" login-section  flex justify-end  pr-5">
            <div className=" flex gap-x-[19px] justify-between items-center">
              <div className="left ">
                <p className=" text-white">Login/sign up</p>
              </div>
              <div className="right">
                <img
                  src="/website/home/loginIcon.png"
                  className=" w-[50px] h-[50px] object-cover"
                  alt="user-icon"
                />
              </div>
            </div>
          </div>
        </div>

        <ul className="ms-5 flex flex-col justify-between gap-x-[30px]  ">
          <li className="  cursor-pointer hover:text-[#1F6CAB]">
            <NavLink
              to="/"
              end
              className={({ isActive }) => (isActive ? "text-[#1F6CAB]" : "")}
            >
              Home
            </NavLink>
          </li>
          <li className=" cursor-pointer hover:text-[#1F6CAB]">
            <NavLink
              to="/about"
              className={({ isActive }) => (isActive ? "active" : "")}
            >
              About
            </NavLink>
          </li>
          <li className=" cursor-pointer hover:text-[#1F6CAB]">
            <NavLink
              to="/blog"
              className={({ isActive }) => (isActive ? "active" : "")}
            >
              Blog
            </NavLink>
          </li>
          <li className=" cursor-pointer hover:text-[#1F6CAB]">
            <NavLink
              to="/get-the-app"
              className={({ isActive }) => (isActive ? "active" : "")}
            >
              Get the app
            </NavLink>
          </li>
          <li className="cursor-pointer hover:text-[#1F6CAB]">
            <NavLink
              to="/add-lab"
              className={({ isActive }) => (isActive ? "active" : "")}
            >
              Add lab
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default WebsiteHeader;
