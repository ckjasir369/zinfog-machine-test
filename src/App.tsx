import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import DashboardLayout from "./components/dashboard/DashboardLayout";
import WebsiteLayout from "./components/website/WebsiteLayout";
import WebsiteLandingPage from "./pages/website/WebsiteLandingPage";
import DashboardLandingPage from "./pages/dashboard/DashboardLandingPage";
import Login from "./pages/login/Login";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<WebsiteLayout />}>
          <Route index element={<WebsiteLandingPage />} />
        </Route>
        <Route path="/login" element={<Login />}/>
        <Route path="/dashboard" element={<DashboardLayout />}>
          <Route index element={<DashboardLandingPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
