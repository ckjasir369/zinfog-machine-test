/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";
import "./DashboardLandingPage.scss";
import { Document, Page } from "react-pdf";

import Modal from "react-modal";

const patientData = [
  {
    bill_date: "2023-05-30",
    bill_no: "1123",
    date: "2023-05-24",
    doctor: "AJITH",
    eta: "2023-05-24 17:28:04",
    has_result: true,
    hospital_id: "546AVBG",
    is_eta_crossed: false,
    lab: "Aswini Diagnostics-Calicut",
    order_id: 13937,
    orderline_id: 47039,
    order_ref: "A13672",
    patient_details: {
      age: 0,
      bill_date: "2023-05-30",
      bill_no: "1123",
      gender: false,
      patient_name: "VIDHYA KT",
    },
    patient_name: "VIDHYA KT",
    result: [
      {
        attachment: "https://www.africau.edu/images/default/sample.pdf",
        id: 124242,
        line_id: 47039,
      },
    ],
    status: "Ready",
    test_name: "Lipid Profile - Fasting",
  },
  {
    bill_date: "2023-05-29",
    bill_no: "9833",
    date: "2023-05-24",
    doctor: "AJITH",
    eta: "2023-05-24 17:28:04",
    has_result: false,
    hospital_id: "546AVBG",
    is_eta_crossed: true,
    lab: "Aswini Diagnostics-Calicut",
    order_id: 13937,
    orderline_id: 47040,
    patient_details: {
      age: 0,
      bill_date: "2023-05-29",
      bill_no: "9833",
      gender: false,
      patient_name: "MIDHUN DAS",
    },
    patient_name: "MIDHUN DAS",
    result: [],
    status: "Lab Dropped",
    test_name: "16s rRNA Sequencing#",
  },
  {
    bill_date: "2023-05-27",
    bill_no: "45122",
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 17:23:19",
    has_result: true,
    hospital_id: "98456",
    is_eta_crossed: false,
    lab: "Aswini Hospital Thrissur",
    order_id: 13936,
    orderline_id: 47038,
    patient_details: {
      age: 0,
      bill_date: "2023-05-27",
      bill_no: "45122",
      gender: false,
      patient_name: "NANCY",
    },
    patient_name: "NANCY",
    result: [
      {
        attachment:
          "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",
        id: 124240,
        line_id: 47038,
      },
    ],
    status: "Ready",
    test_name: "*Rapid Dengue Test (NS1 Ag & IgM/IgG Ab)",
  },
  {
    bill_date: "2023-05-24",
    bill_no: "1123",
    date: "2023-05-24",
    doctor: "AJITH",
    eta: "2023-05-24 17:39:32",
    has_result: false,
    hospital_id: "7877",
    is_eta_crossed: false,
    lab: "Amrita Institute of Medical Sciences",
    order_id: 13934,
    orderline_id: 47036,
    patient_details: {
      age: 0,
      bill_date: "2023-05-24",
      bill_no: "1123",
      gender: false,
      patient_name: " Jose  po",
    },
    patient_name: " Jose  po",
    result: [],
    status: "Lab Dropped",
    test_name: "16s rRNA Sequencing#",
  },
  {
    bill_date: "2023-05-09",
    bill_no: "5465",
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 17:39:32",
    has_result: false,
    hospital_id: "656456",
    is_eta_crossed: false,
    lab: "AZA Diagnostics-Calicut",
    order_id: 13933,
    orderline_id: 47035,
    patient_details: {
      age: 0,
      bill_date: "2023-05-09",
      bill_no: "5465",
      gender: false,
      patient_name: " BASTIN ANIKKAL",
    },
    patient_name: " BASTIN ANIKKAL",
    result: [],
    status: "Lab Dropped",
    test_name: "*Rapid Dengue Test (NS1 Ag & IgM/IgG Ab)",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:55:07",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13902,
    orderline_id: 47002,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:54:14",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13903,
    orderline_id: 47003,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:53:34",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13904,
    orderline_id: 47004,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:52:51",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13905,
    orderline_id: 47005,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:51:47",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13906,
    orderline_id: 47006,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:50:45",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13907,
    orderline_id: 47007,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:49:58",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13908,
    orderline_id: 47008,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:49:12",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13909,
    orderline_id: 47009,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:48:27",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13910,
    orderline_id: 47010,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:47:40",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13911,
    orderline_id: 47011,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: false,
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:46:52",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13912,
    orderline_id: 47012,
    patient_details: {
      age: 0,
      bill_date: false,
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: "2023-05-09",
    bill_no: "877",
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:45:44",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13913,
    orderline_id: 47013,
    patient_details: {
      age: 0,
      bill_date: "2023-05-09",
      bill_no: "877",
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: "2023-05-02",
    bill_no: "54654",
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:42:00",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13914,
    orderline_id: 47014,
    patient_details: {
      age: 0,
      bill_date: "2023-05-02",
      bill_no: "54654",
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: "2023-05-10",
    bill_no: "55544",
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:41:09",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: false,
    order_id: 13915,
    orderline_id: 47015,
    patient_details: {
      age: 0,
      bill_date: "2023-05-10",
      bill_no: "55544",
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
  {
    bill_date: "2023-05-11",
    bill_no: false,
    date: "2023-05-24",
    doctor: "JOHN",
    eta: "2023-05-24 09:40:23",
    has_result: false,
    hospital_id: false,
    is_eta_crossed: true,
    lab: "Amrita Institute of Medical Sciences",
    order_id: 13916,
    orderline_id: 47016,
    patient_details: {
      age: 0,
      bill_date: "2023-05-11",
      bill_no: false,
      gender: false,
      patient_name: "Daya hospital portal",
    },
    patient_name: "Daya hospital portal",
    result: [],
    status: "Lab Dropped",
    test_name: "17 Ketosteroids 24 Hours Urine#",
  },
];

function DashboardLandingPage() {
  const [filters, setFilters] = useState({
    fromDate: "",
    toDate: "",
    referBy: "",
    patientName: "",
    hospitalId: "",
    status: "",
    billNo: "",
    searchQuery: "",
  });

  const [filteredData, setFilteredData] = useState(patientData);

  const handleFilterChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;

    console.log("loggg val", value);
    console.log("loggg name", name);

    setFilters((prevFilters) => {
      const updatedFilters = { ...prevFilters, [name]: value };
      filterData(updatedFilters);
      return updatedFilters;
    });
  };

  const filterData = (filters: any) => {
    let data = patientData;

    if (filters.fromDate) {
      data = data.filter(
        (patient: any) => new Date(patient.date) >= new Date(filters.fromDate)
      );
    }
    if (filters.toDate) {
      data = data.filter(
        (patient: any) => new Date(patient.date) <= new Date(filters.toDate)
      );
    }
    if (filters.referBy) {
      data = data.filter((patient: any) =>
        patient.doctor.includes(filters.referBy)
      );
    }
    if (filters.patientName) {
      // Convert the billNo filter to a string to match parts of the order_id
      const patientNameStr = filters.patientName.trim();
      data = data.filter((patient: any) =>
        patient.patient_name.toString().toLowerCase().includes(patientNameStr)
      );
      console.log("Filtered by billNo:", data);
    }

    if (filters.hospitalId) {
      // Convert the billNo filter to a string to match parts of the order_id
      const hospitalIdStr = filters.hospitalId.trim();
      data = data.filter((patient: any) =>
        patient.hospital_id.toString().toLowerCase().includes(hospitalIdStr)
      );
      console.log("Filtered by billNo:", data);
    }
    if (filters.status) {
      data = data.filter((patient: any) => patient.status === filters.status);
    }

    if (filters.billNo) {
      // Convert the billNo filter to a string to match parts of the order_id
      const billNoStr = filters.billNo.trim();
      console.log("billNo to filter (as string):", billNoStr);
      data = data.filter((patient: any) =>
        patient.order_id.toString().includes(billNoStr)
      );
      console.log("Filtered by billNo:", data);
    }
    if (filters.searchQuery) {
      const query = filters.searchQuery.toLowerCase();
      data = data.filter(
        (patient: any) =>
          patient.patient_name.toLowerCase().includes(query) ||
          patient.test_name.toLowerCase().includes(query) ||
          patient.doctor.toLowerCase().includes(query)
      );
    }

    setFilteredData(data);
  };

  const clearData = () => {
    const resetFilters = {
      fromDate: "",
      toDate: "",
      referBy: "",
      patientName: "",
      hospitalId: "",
      status: "",
      billNo: "",
      searchQuery: "",
    };
    setFilters(resetFilters);
    setFilteredData(patientData); // Reset filtered data to original data set
  };

  const [isFilterVisible, setIsFilterVisible] = useState(false);
  const [selectedDocument, setSelectedDocument] = useState("");

  // Toggle the visibility state
  const handleToggleFilter = () => {
    setIsFilterVisible(!isFilterVisible);
  };

  const getUniqueDoctorNames = () => {
    const doctorNames = patientData.map((patient) => patient.doctor);
    return Array.from(new Set(doctorNames)); // Remove duplicates
  };

  const getUniqueStatuses = () => {
    const statuses = patientData.map((patient) => patient.status);
    return Array.from(new Set(statuses)); // Remove duplicates
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [selectedPatient, setSelectedPatient] = useState<any>({});
  const [selectedPOrderId, setSelectedPOrderId] = useState<any>({});

  const [isPatientModalOpen, setIsPatientModalOpen] = useState(false);

  const [isCommentModalOpen, setIsCommentModalOpen] = useState(false);

  const openModal = async (pdfUrl: any) => {
    try {
      setSelectedDocument(pdfUrl);
      // Use pdf object to display the PDF in the modal
      setIsModalOpen(true);
    } catch (error) {
      console.error("Error loading PDF:", error);
    }
  };

  const closeCommentModalOpen = () => {
    setIsCommentModalOpen(false);
  };
  const openCommentModalOpen = async (item: any) => {
    try {
      setSelectedPOrderId(item);
      setIsCommentModalOpen(true);
    } catch (error) {
      console.error("Error loading PDF:", error);
    }
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };
  const openPatientModal = async (item: any) => {
    try {
      setSelectedPatient(item?.patient_details);
      setIsPatientModalOpen(true);
    } catch (error) {
      console.error("Error loading PDF:", error);
    }
  };

  const closePatientModal = () => {
    setIsPatientModalOpen(false);
  };

  console.log("selectedDocument", selectedDocument);

  return (
    <>
      <section className="dashboard-home-section  bg-[#E4FBFB] p-[15px]  w-full">
        <div className="heading-section lg:flex justify-between  custom-container w-full">
          <div className="left lg:w-[25%]">
            <h4 className=" text-[#1F6CAB] font-[500] text-[25px]">
              Patient Reports
            </h4>
          </div>
          <div className="right flex justify-end   lg:w-[75%]">
            <div className="flex  gap-x-[16px]">
              <button
                onClick={handleToggleFilter}
                className="w-[80px] lg:w-[150px] h-[30px] ps-[7px] pr-[7px] flex justify-between items-center text-[12px] lg:text-[20px] font-[500] text-white bg-[#1F6CAB] rounded-[5px] border border-[#1F6CAB]"
              >
                Apply Filter
                <img
                  src="/dashboard/home/filterIcon.png"
                  className=" w-[12px] lg:w-[16px] object-cover"
                  alt="filter-logo"
                />
              </button>
              <div className="right relative lg:w-[450px] h-[30px]">
                <input
                  type="text"
                  placeholder="Search by Doctor Name/ Patient Name/ Test Name..."
                  className=" w-full p-3 outline-none  h-[30px] rounded border  border-[#1F6CAB] bg-[#F4F4F4] "
                  onChange={handleFilterChange}
                  name="searchQuery"
                  value={filters.searchQuery}
                />
                <img
                  src="/website/home/searchIcon.png"
                  className=" absolute top-1/2 right-0 transform -translate-x-1/2 -translate-y-1/2  w-[18px]  object-cover"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
        <div
          className={`mt-[18px]  bg-[#fcfcfc] text-[#505050] rounded  overflow-hidden transition-all duration-500 ease-in-out ${
            isFilterVisible
              ? "max-h-[220px] pt-[9px]   border border-[#828282]"
              : "max-h-0"
          }`}
        >
          {isFilterVisible && (
            <>
              <div className="ps-[48px] pr-[48px] pb-[12px] grid grid-cols-3 gap-[10px] ">
                <div className="from-date flex justify-between items-center">
                  <label
                    htmlFor="from-date"
                    className="flex items-center gap-x-[46px]"
                  >
                    From Date
                    <input
                      type="date"
                      id="from-date"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      name="fromDate"
                      onChange={handleFilterChange}
                      value={filters.fromDate}
                    />
                  </label>
                </div>
                <div className="from-date flex justify-between items-center">
                  <label
                    htmlFor="to-date"
                    className="flex items-center gap-x-[66px]"
                  >
                    To Date
                    <input
                      type="date"
                      id="to-date"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      name="toDate"
                      onChange={handleFilterChange}
                      value={filters.toDate}
                    />
                  </label>
                </div>
                <div className="refer-by flex justify-between items-center">
                  <label
                    htmlFor="refer-by"
                    className="flex justify-between items-center gap-x-[60px]"
                  >
                    Refer By
                    {/* <input
                      type="date"
                      id="refer-by"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                    /> */}
                    <select
                      name="referBy"
                      id="refer-by"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      onChange={handleFilterChange}
                      value={filters.referBy}
                    >
                      <option value="">Select Doctor</option>
                      {getUniqueDoctorNames().map((name) => (
                        <option key={name} value={name}>
                          {name}
                        </option>
                      ))}
                    </select>
                  </label>
                </div>
                <div className="patient-name flex justify-between items-center">
                  <label
                    htmlFor="refer-by"
                    className="flex items-center gap-x-[25px]"
                  >
                    Patient Name
                    <input
                      type="text"
                      placeholder="patient name"
                      id="patient-name"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      name="patientName"
                      onChange={handleFilterChange}
                      value={filters.patientName}
                    />
                  </label>
                </div>
                <div className="hospital-id flex justify-between items-center">
                  <label
                    htmlFor="hospital-id "
                    className="flex items-center gap-x-[46px]"
                  >
                    Hospital ID
                    <input
                      type="text"
                      placeholder="hospital id "
                      id="hospital-id "
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      name="hospitalId"
                      onChange={handleFilterChange}
                      value={filters.hospitalId}
                    />
                  </label>
                </div>
                <div className="Status flex justify-between items-center">
                  <label
                    htmlFor="Status"
                    className="flex justify-between items-center gap-x-[75px]"
                  >
                    Status
     
                    <select
                      name="status"
                      id="status"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      onChange={handleFilterChange}
                      value={filters.status}
                    >
                      <option value="">Select Status</option>
                      {getUniqueStatuses().map((status) => (
                        <option key={status} value={status}>
                          {status}
                        </option>
                      ))}
                    </select>
                  </label>
                </div>
                <div className="bill-no flex justify-between items-center">
                  <label
                    htmlFor="bill-no"
                    className="flex items-center gap-x-[78px]"
                  >
                    Bill No
                    {/* <input
                      type="date"
                      id="refer-by"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                    /> */}
                    <input
                      type="text"
                      placeholder="Bill No"
                      id="bill-no"
                      className="border-2 ps-5 pr-5 pt-2 pb-2 w-[249px]"
                      name="billNo"
                      onChange={handleFilterChange}
                      value={filters.billNo}
                    />
                  </label>
                </div>
              </div>
              <div className="p-[18px] flex  justify-end bg-[#E4FBFB] border">
                <button
                  onClick={clearData}
                  className=" bg-[#F46C09] w-[106px] h-[36px] text-white rounded-[5px] flex justify-center items-center gap-x-[10px]"
                >
                  <img
                    src="/dashboard/home/close-icon.png"
                    alt=""
                    className=" w-[13px] h-[13px] object-contain"
                  />{" "}
                  Clear
                </button>
              </div>
            </>
          )}
        </div>
        <div className="table-section bg-[#fcfcfc]  pt-5">
          <div className="custom-container">
            <table className="full-table w-full  border  ">
              <thead className=" shadow-md  ">
                <tr className="">
                  <th className="px-6 py-2 text-left">Order No</th>
                  <th>Date</th>
                  <th>Patient Name</th>
                  <th>Hospital Id</th>
                  <th>Test Name</th>
                  <th>Doctor Name</th>
                  <th>ETA</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {filteredData &&
                  filteredData?.map((item: any, index: any) => (
                    <tr key={index} className=" border p-2">
                      <td>{item?.order_id}</td>
                      <td>{item?.date}</td>
                      <td
                        onClick={() => {
                          openPatientModal(item);
                        }}
                        className=" cursor-pointer"
                      >
                        {item?.patient_name}
                      </td>
                      <td>{item?.hospital_id}</td>
                      <td>{item?.test_name}</td>
                      <td>{item?.doctor}</td>
                      <td>{item?.eta}</td>
                      <td>
                        <div
                          className={`${
                            item?.status === "Ready"
                              ? "bg-[#89FFAA]"
                              : item?.status === "Lab Dropped"
                              ? "bg-[#F2A38A]"
                              : "bg-[#E7F880]"
                          }  rounded-[12px] w-[122px] min-h-[23px] ps-5`}
                        >
                          {item?.status}
                        </div>
                      </td>
                      <td>
                        <div className=" flex gap-x-[30px] justify-end items-center">
                          {item?.result?.length > 0 && (
                            <img
                              src="/dashboard/home/download.png"
                              alt="download-icon"
                              className=" w-[25px] h-[25px] object-contain cursor-pointer"
                              onClick={() => {
                                openModal(item?.result[0]?.attachment);
                              }}
                            />
                          )}

                          <img
                           onClick={() => {
                            openCommentModalOpen(item?.order_id);
                          }}
                            src="/dashboard/home/comment.png"
                            alt="download-icon"
                            className=" w-[25px] h-[25px] object-contain cursor-pointer"
                          />
                        </div>
                      </td>
                    </tr>
                  ))}
                {filteredData?.length === 0 && (
                  <tr>
                    <p className=" p-[40px] text-[20px]">No data Found</p>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </section>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        className="modal"
        overlayClassName="modal-overlay"
      >
        <h2>Report Preview (Patient name)</h2>
        <Document file={selectedDocument}>
          <Page pageNumber={1} />
        </Document>
        <button className="close-btn" onClick={closeModal}>
          Close
        </button>
      </Modal>
      <Modal
        isOpen={isPatientModalOpen}
        onRequestClose={closePatientModal}
        className="modal bg-[#B8FACE] border border-black w-[434px] min-h-[81px] text-[#505050]"
        overlayClassName="modal-overlay"
      >
        <div className="">
          <h4 className=" text-[18px] font-[700]">
            {" "}
            {selectedPatient?.patient_name}
          </h4>
          <div className="div mt-[0] flex items-center gap-x-[20px]">
            <p>Age : {selectedPatient?.age} Y</p>
            <p>Gender : {selectedPatient?.gender}</p>
          </div>
          <div className="div mt-[10px] flex items-center  justify-between">
            <p>Bill No : {selectedPatient?.bill_no}</p>
            <p>Bill date: {selectedPatient?.bill_date}</p>
          </div>
        </div>
      </Modal>
      <Modal
        isOpen={isCommentModalOpen}
        onRequestClose={closeCommentModalOpen}
        className=" comment-modal bg-[#E4FBFB] border border-black w-[434px] min-h-[81px] text-[#505050]"
        overlayClassName="modal-overlay"
      >
        <div className="">
<div className=" flex justify-between">
            <h4 className=" text-[18px] font-[700]">
  {selectedPOrderId}
            </h4>
            <button onClick={closeCommentModalOpen} className=" bg-[#1F6CAB] rounded-[5px] w-[20px] h-[20px] flex justify-center items-center" > <img
                    src="/dashboard/home/close-icon.png"
                    alt=""
                    className=" w-[13px] h-[13px] object-contain"
                  /> </button>
</div>
 <textarea className=" w-full h-[76px] p-[7px] outline-none" name="message" id="message" placeholder="Type your messages here">
 </textarea>
 <div className="flex justify-end mt-3">
 <button className=" bg-[#1F6CAB] rounded-[5px] w-[44px]  text-center text-white">send</button>
 </div>
        </div>
      </Modal>
    </>
  );
}

export default DashboardLandingPage;
