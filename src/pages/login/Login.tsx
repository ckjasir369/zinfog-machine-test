/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const navigate = useNavigate();

  const validCredentials = {
    user_name: "aswini@b2b.com",
    password: "123",
  };
  const validCredentials2 = {
    user_name: "daya@b2b.com",
    password: "123",
  };

  const handleSubmit = (e: any) => {
    e.preventDefault(); // Prevent the form from refreshing the page

    // Check if entered credentials match the predefined ones
    if (
      username === validCredentials.user_name ||
      (username === validCredentials2.user_name &&
        password === validCredentials.password) ||
      password === validCredentials2.password
    ) {
      navigate("/dashboard"); // Navigate to the home page
    } else {
      setError("Wrong Credentials! Your email Id or password entered is wrong");
    }
  };

  const handleUsernameChange = (e: any) => {
    setUsername(e.target.value);
    if (error) setError(""); // Clear error if it exists
  };

  const handlePasswordChange = (e: any) => {
    setPassword(e.target.value);
    if (error) setError(""); // Clear error if it exists
  };

  return (
    <section
      className="login-section min-h-screen flex justify-center items-center"
      style={{
        backgroundImage: `url('/dashboard/login/BgImage.png')`,
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
    >
      <div className="login-box m-[11px]  lg:m-[91px]">
        <div className="card relative  bg-[#FFFFFF] rounded-[45px] w-full lg:w-[603px] ps-[10px] pr-[10px] pt-[51px] pb-[20px]  lg:ps-[75px] lg:pr-[75px] lg:pt-[81px] lg:pb-[39px]">
          <div className="brand-logo flex justify-center">
            <NavLink to={"/"}>
              <img
                src="/dashboard/login/Logo.png"
                className=" lg:w-[196px] object-cover"
                alt="brand-logo"
              />
            </NavLink>
          </div>
          <p className=" mt-[20px] lg:mt-[51px] text-center text-[#1F6CAB] font-[700] text-[18px] lg:text-[24px] leading-[30px]">
            Report Download Portal
          </p>
          <div className="login-input-box mt-[22px] bg-[#00D3EB] rounded-[20px] p-[20px] lg:p-[36px]">
            <h5 className="text-[24px] font-[400] text-center text-[#1F6CAB]">
              Login
            </h5>
            <div className="values-box mt-[18px]">
              <form onSubmit={handleSubmit}>
                <label
                  htmlFor="username"
                  className=" text-[18px] text-[#505050] font-[300]"
                >
                  Username
                  <input
                    type="email"
                    required
                    value={username}
                    onChange={handleUsernameChange}
                    placeholder="Enter email Id"
                    className=" outline-none w-full rounded-[5px] p-[10px]"
                  />
                </label>
                <label
                  htmlFor="password"
                  className=" text-[18px] text-[#505050] font-[300] mt-[5px]"
                >
                  Username
                  <input
                    required
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={handlePasswordChange}
                    className=" outline-none w-full rounded-[5px] p-[10px]"
                  />
                </label>
                <div className=" flex justify-center">
                  <button
                    type="submit"
                    className=" mt-[20px] bg-[#1F6CAB] hover:bg-[#1f6cab80]  border-[#1F6CAB] text-white w-[138px] h-[36px] text-center rounded-[5px] "
                  >
                    SUBMIT
                  </button>
                </div>
              </form>
              <p className=" mt-[20px] text-center text-[#505050] underline">
                Reset Password
              </p>
            </div>
          </div>
          <div className=" h-[50px] mt-[14px]">
            {error && (
              <div className=" h-[50px]">
                <p className=" text-[#FA0000] font-[500] text-center ">
                  {error}
                </p>
              </div>
            )}
          </div>
          <div className="mt-[20px] lg:mt-[80px]  flex justify-center">
            <div className=" flex justify-between gap-x-[6px] cursor-pointer">
              <a
                href="tel:+919288008801"
                className="flex items-center gap-x-[6px]"
              >
                <img
                  src="/dashboard/login/Phone.png"
                  alt="phone-logo"
                  width={30}
                  height={30}
                  className="w-[30px] object-contain"
                />
                <p className="text-[24px] text-[#1F6CAB] font-[700]">
                  (+91) 9288008801
                </p>
              </a>
            </div>
          </div>
          <p className=" mt-[84px] text-center text-[#505050] font-[500] text-[14px]">
            I hereby agree and accept the{" "}
            <span className=" text-[#1F6CAB]">Terms of service</span> and{" "}
            <span className=" text-[#1F6CAB]">Privacy policy</span>
          </p>
          <div className="plus-small absolute top-[45px] left-[35px]">
            <img
              src="/dashboard/login/PlusIcon.png"
              alt="plus-icon"
              className="h-[30px] object-cover"
            />
          </div>
          <div className="plus-medium absolute top-[132px] right-[42px]">
            <img
              src="/dashboard/login/PlusIcon.png"
              alt="plus-icon"
              className="h-[30px] lg:h-[60px] object-cover"
            />
          </div>
          <div className="plus-big absolute bottom-[96px] left-[15px]">
            <img
              src="/dashboard/login/PlusIcon.png"
              alt="plus-icon"
              className="h-[50px] lg:h-[100px] object-cover"
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default Login;
