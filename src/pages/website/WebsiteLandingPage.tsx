import React from 'react'
import HomeBanner from '../../components/website/home/homebanner/HomeBanner'
import PopularLabs from '../../components/website/home/popularlabs/PopularLabs'
import PopularTests from '../../components/website/home/populartests/PopularTests'
import HowWeWork from '../../components/website/home/howwework/HowWeWork'
import PickLeft from '../../components/website/home/pickleft/PickLeft'
import PopularPackages from '../../components/website/home/popularpackages/PopularPackages'
import OfferSection from '../../components/website/home/offersection/OfferSection'
import LatestBlogs from '../../components/website/home/latestblogs/LatestBlogs'
import ContactUs from '../../components/website/home/contactussection/ContactUs'
import FaqSection from '../../components/website/home/faqsection/FaqSection'

function WebsiteLandingPage() {
  return (
    <section className='home-section mt-[80px]'>
      <HomeBanner/>
   <PopularLabs/>
   <PopularTests/>
   <HowWeWork/>
   <PickLeft/>
   <PopularPackages/>
   <OfferSection/>
   <PickLeft/>
   <LatestBlogs/>
   <ContactUs/>
   <FaqSection/>
    </section>
  )
}

export default WebsiteLandingPage